Some instructions for editing this project!

=====================Using GIT!========================
Tutorial for using git commandline to push changes to bitbucket.
Use
"git clone https://tokendeveloper@bitbucket.org/tokendeveloper/webprogramming_finalproject.git"
to clone the repo.

If you made any changes: 

1. Navigate to the final project file directory:

2. git add --all
-Adds files that have been changed to get it ready for commits.

3. git commit -m " "
-Commit the changes and get it ready to be pushed into bitbucket repo. 
-Type in a brief text between the quotation marks that best describes the changes you have made.

4. git push origin master
-Push the changes onto bitbucket on the master branch.

=======BRANCHING=============================================
To avoid merge conflicts, you should create a new branch in your local repository and work on that branch.
Here's a tutorial on how to create a branch in your local repo:

In the git commandline, navigate to the local directory where your git is initiated (if you cloned a repository, which you would have done to get this project, it would 
already have a git init file).

-Type in "git branch" to check what branches exist in the current project. It'll most likely just say "master" at this point.
-Type "git branch <name of new branch>" to create the new branch.
-Type "git checkout <name of branch>" to tell git to look at the particular branch you want to work on.

Congrats, you are now working on a new branch. 
If you want to push the branch into the bitbucket repo, you can type in:

git push origin <branch name>

This will push the new branch that you created into the bitbucket repo. 
You can check 

--Somethings to think about --
Merge conflicts are scary. 
Merge conflicts happen because two or more people try to push 2 different changes on the same file on the same branch.
E.g. If 2 of us worked on the index.html file, where one of us named one of the div ID as "about" and the other named that same ID as "about-us",
GIT would not know how to resolve this conflict.

To avoid this:
-Work on another branch! Loop above for how to create a new branch of the same project. 
-Communicate with your teammates about what file(s) you're going to work on.
-Enter command "git pull" to make sure that the changes you are making are done on the latest copy of the project.
-Talk to your teammates when you've made any major changes (e.g. editing functions, adding functions, etc) so that they are aware.
-You can add to the existing code, but try not to make changes that directly affect other people's code without their knowlege.
	-E.g. renaming functions, variables, IDs, classes, etc. 
	-This way, when the code is broken somewhere, the original person in charge of writing that code can debug code faster.

====================HTML FILES ========================

Copy and paste elements from the templateHTML.txt file in each of the HTML pages. It's a template for what goes inside the head element, navigation, footer, and script file. 

For each HTML page file:
Have an ID for the body tag. Name the ID as the page name. 
	i.e. For Home page, id="home". For Menu page, id='menu'

Add a class for the body tag, named 'blank'. This allows the style.css and script.js to render the fade-in effect.


=================CSS===============================
The style is separated into 2 files:

univeresal-style.css
	This stylesheet defines the styling for elements that consistently exist for each of the HTML pages.
		-i.e. navigation, footer, "back-to-top" button, etc. 

style.css
	This stylesheet defines the styling for elements that are more page specific. Before you style any new sections, leave a comment at the top saying what page and section the style is targeting.


================JavaScript==================================

If you want to add any script for interactivity, edit script.js.
If a page specific script is getting too long or it's cluttering the script.js file, it might be better to create a separate js file.
For example, I created a menu.js file to store the logic and objects for menu items.

Naming conventions:
For functions, please use a name that best describes what they are intended to do. This way, debugging may be easier, as the intentions of your code is clearer. 

It's good practice to create functions that do very specific things, rather than having a function that does too many things.

The code is more mobile and reusable this way, and debugging would be easier. Otherwise,
the error messages may be too vague, saying "this function won't work", but we have no idea where in the function the error is coming from.

If you feel like the code is becoming complicated, always comment what the code is doing. This improves readability. 


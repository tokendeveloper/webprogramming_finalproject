//==============================Menu Page==============================================================//
//Create menu item object
//Put menu item object in list
//Use loop to loop through list and print out each item

//Menu Class
//I don't think there's much point to making a class for this,
//but I wanted to practice making objects using classes.


class MenuItem {

	constructor(name, description, price){
		this.name = name;
		this.description = description;
		this.price = price;
	}

}

//Menu Variables

let appetizerList = [];
const appetizerDiv = document.getElementById("menu-appetizer-list");

let burgerList = [];
const burgerDiv = document.getElementById("menu-burger-list");

let sideList = [];
const sideDiv = document.getElementById("menu-side-list");

let drinkList = [];
const drinkDiv = document.getElementById("menu-drink-list");

let dessertList = [];
const dessertDiv = document.getElementById("menu-dessert-list");

//Function to add menu item to existing array.
function addMenuItem(name, description, price, array) {
	let menuItem = new MenuItem(name, description, price)
	array.push(menuItem);
}

//Appetizer Menu Item Object//
addMenuItem('Definitely an Appetizer', 'This is definitely an appetizer.', 7, appetizerList);
addMenuItem('Kind of an Appetizer', 'This is sort of like an appetizer.', 8, appetizerList);
addMenuItem('Maybe it\'s an Appetizer', 'This could be an appetizer. Maybe.', 7, appetizerList);
addMenuItem('Definitely Vegetables', 'This is definitely not a burger. It\'s salad.', 9, appetizerList);

//Burger Menu Item Objects;
addMenuItem('Definitely A Beef Burger', 'This burger definitely has beef in it.', 13, burgerList);
addMenuItem('Definitely A Chicken Burger', 'This burger definitely has chicken in it.', 13, burgerList);
addMenuItem('Definitely Not A Vegetarian Burger', 'This burger has no vegetables in it.', 14, burgerList);
addMenuItem('Definitely Vegetarian', 'This burger has ONLY vegetables in it.', 12, burgerList);

//Sides Menu Item Objects;
addMenuItem('Definitely Fries', 'This is definitely fries. Potatoes.', 6, sideList);
addMenuItem('Definitely Ring of Onions', 'This might contain onions. Mostly rings.', 5, sideList);
addMenuItem('Definitely Egg. Fried.', 'Eggs. Fried. Definitely.', 5, sideList);
addMenuItem('Definitely Mysterious.', 'Some mysterious looking pieces of goodness.', 6, sideList);

//Sides Menu Item Objects;
addMenuItem('Definitely A Drink', 'This is definitely a liquid.', 2, drinkList);
addMenuItem('Might Be Drinkable', 'This is probably drinkable.', 6, drinkList);
addMenuItem('Definitely Not Pepsi', 'It\'s Coca Cola.', 6, drinkList);
addMenuItem('Definitely Not Not Pepsi', 'This is probably Pepsi.', 6, drinkList);

//Dessert Menu Item Objects;
addMenuItem('Definitely A Dessert', 'This is definitely not a desert.', 5, dessertList);
addMenuItem('Definitely Sweet', '"You are beautiful" - <em>James Blunt</em>', 6, dessertList);
addMenuItem('Definitely Suite', 'It\'s like eating a symphony.', 7, dessertList);
addMenuItem('Definitely Sugar', 'Pretty sugary I must say.', 5, dessertList);

//Function to print menu items into the DOM;
function printMenuItem(div, list){
	for(i=0; i<appetizerList.length; i++){
		div.innerHTML += `<strong>${list[i].name}:</strong>	$${list[i].price}  <br> ${list[i].description} <br>  <br>`;
	}
}

//Call functions to print menu items onto the DOM.

printMenuItem(appetizerDiv, appetizerList);
printMenuItem(burgerDiv, burgerList);
printMenuItem(sideDiv, sideList);
printMenuItem(drinkDiv, drinkList);
printMenuItem(dessertDiv, dessertList);

let menuList = document.getElementsByClassName('menu');




//Carousel menu function -Right Direction.
$('#rightAngle').click(scrollRight);

var rightAngleText = document.getElementById('rightAngleText');


function scrollRight(){
	//Start at -1 so that the loop doesn't break when it gets to the last item.
	//index of -1 returns the last item in the array.
	for(i=-1;i<menuList.length-1;i++){
		if(!$('.menu').eq(i).hasClass('hidden')){
			$('.menu').addClass('hidden');

			//Add logic to dynamically change menu navigation text next to the
			//arrows.

			//if i is on the 4th item, display the 1st menu title next to the 
			//right arrow.
			if(i === menuList.length - 2){
				rightAngleText.innerHTML ='To ' + $('.menuTitle').eq(0).html();
				leftAngleText.innerHTML = 'To ' + $('.menuTitle').eq(i).html();
			} else {
				rightAngleText.innerHTML ='To ' + $('.menuTitle').eq(i+2).html();
				leftAngleText.innerHTML = 'To ' + $('.menuTitle').eq(i).html();
			}
			return $( '.menu').eq(i+1).removeClass('hidden');
		} 
	}
}




//Carousel menu function -Left Directionn.
$('#leftAngle').click(scrollLeft);

var leftAngleText = document.getElementById('leftAngleText');

function scrollLeft(){
	for(i=4;i<menuList.length;i--){
		if(!$('.menu').eq(i).hasClass('hidden')){
			$('.menu').addClass('hidden');

			//Add logic to dynamically change menu navigation text next to the
			//arrows.
			rightAngleText.innerHTML = 'To ' +$('.menuTitle').eq(i).html();
			leftAngleText.innerHTML = 'To ' + $('.menuTitle').eq(i-2).html();

			return $( '.menu').eq(i-1).removeClass('hidden');
		}
	}
}

//Keybindings for carousel menu control
//https://stackoverflow.com/questions/17593391/how-to-add-onkeydown-to-body
document.body.onkeydown = function(e){
	if(e.keyCode === 39 /*Right Arrow*/){
		scrollRight();
	} else if (e.keyCode === 37 /*Left Arrow*/){
		scrollLeft();
	} 
}

leftAngleText.innerHTML = 'To ' + $('.menuTitle').eq(-1).html();
rightAngleText.innerHTML = 'To ' + $('.menuTitle').eq(1).html();
//Display of html is initially none. (add class "blank")
//On page load, transition to normal display (take away class "blank")
window.onload = page_load

//Put function in here that you want to run when page loads.
function page_load() {
	fadeIn();
	$("#toTop").click(function() {
		scrollToAnchor('top');
	});

	$("#toHours").click(function(){
		scrollToAnchor('hours');
	})

	//Retrieved from https://stackoverflow.com/questions/5347357/jquery-get-selected-element-tag-name
	//Use jquery to do a scroll animation.
	//scrollToAnchor takes a parameter "nameattr"(name attribute).
	//variable anchor is stored as jquery's attribute selector, which selects anchor tag's name attribute (in this form: $("[attribute~='value']") ).
	//$('html, body') targets the DOM object "html" and "body", and applies the animate method.
	//the method takes an object key-value pair and string value for speed of scroll animation as the parameters.
	//the object key is "scrollTop, and it takes the value pair "anchor.offset()",
	//which is a method used to offset the page to the defined position (in this case, it searches for the anchorr tag).
	//function is called on the jquery method ".click", which takes a call-back function as param to call the scrollToAnchor funciton.
	function scrollToAnchor(nameattr){
		var anchor = $("a[name='"+ nameattr +"']");
		$('html,body').animate({scrollTop: anchor.offset().top},'slow');
	}

	// Intended to remove the "blank" class from body tag after .5seconds.
	// This allows for the css to create the "fade-in" effect uniformly when the page loads.
	// Body Animation
	function fadeIn() {
		setTimeout(function () {
			document.body.className = '';
		}, 500)
	}

	//Gift Card Gallery
	var imageDisp = document.getElementById("giftcard-display");
    //getting the elements needed
    var button25 = document.getElementById("gc-25");
    var button50 = document.getElementById("gc-50");
    var button100 = document.getElementById("gc-100");

    var card25 = document.getElementById("card25");
    var card50 = document.getElementById("card50");
    var card100 = document.getElementById("card100");
    //calling functions when radio button clicked 
    button25.onclick = display25;
    button50.onclick = display50;
    button100.onclick = display100;
    //defining above functions
    function display25(){
    	imageDisp.src = card25.src;
    }
    function display50(){
    	imageDisp.src = card50.src;
    }
    function display100(){
    	imageDisp.src = card100.src;
    }
};

//-- events validation ---------
jQuery(document).ready(function(){
	lockButtons(true);
	jQuery(function() {
		jQuery("#eventDate").datepicker({
			minDate: 1, // you can't reserve the same day
			maxDate: "+1M +10D",
			dateFormat: "mm/dd/yy",
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			buttonText: "Select date",
			// blur workaround https://stackoverflow.com/questions/1814292/jqueryui-datepicker-fires-inputs-blur-before-passing-date-avoid-workaround
			onSelect: function(dateText) {isValid = validateField(jQuery(this), jQuery(this).attr('name'))},
			beforeShowDay: function(date){ return [date.getDay() != 0, '']}
			 // usees before show day to disable sunday, when we are closed
			 // http://api.jqueryui.com/datepicker/#option-altField
			 // https://forum.jquery.com/topic/how-can-i-disable-sunday-s-on-datepicker
			});
	});

	jQuery("#contactForm").submit(contactFormValidate);

	var formWz = jQuery(".formWizard.active *");
	var formStep = jQuery(".formWizard.active").index();
	var ctr = 0;
	var validControl;

	var stepOne = {
		fName: jQuery("#guestFName"),
		lName: jQuery("#guestLName"),
		email: jQuery("#guestEmail"),
		phone: jQuery("#guestCNumber")
	};

	var stepTwo = {
		eventType: jQuery("#otherEvent"),
		eventDate: jQuery("#eventDate"),
		eventTimeFrom: jQuery("#fromTime"),
		eventTimeTo: jQuery("#toTime"),
	};

	var stepThree = {
		numOfGuests: jQuery("#guestSize"),
		specialRequests: jQuery("#specialRequests"),
		eventTerms: jQuery("#eventTerms")
	};

	seqValidate(formWz, formStep);

	// step button event
	jQuery(".step-btn").click(function(){
		var dir = jQuery(this).text().toLowerCase();
		formWizard(dir);
		formWz = jQuery(".formWizard.active *");
		formStep = jQuery(".formWizard.active").index();
		seqValidate(formWz, formStep);
		// lock the buttons as soon as you reach the next step
		if(dir === "next" && stepOne.valid === true)
		{
			lockButtons(true);
		} else if(dir === "next" && stepTwo.valid === true) {
			lockButtons(true);
		} else {
			lockButtons(false);
		}
	});

	function seqValidate(step, index){
		step.filter(":input", ":textarea", ":select").each(function(){
			var isValid;
			var $form = jQuery(this);

			$form.blur(function(){
				console.log('blur');

				isValid = validateField($form, $form.attr('name'));

				if(isValid) {
					//check which step you are in
					if(index == 0){
						if(stepOne.fName.hasClass("valid-field") == true &&
							stepOne.lName.hasClass("valid-field") == true &&
							stepOne.email.hasClass("valid-field") == true &&
							stepOne.phone.hasClass("valid-field") == true) {
							lockButtons(false);
						stepOne.valid = true;
					}
				} else if(index == 1) {
					if(stepTwo.eventType.hasClass("valid-field") == true &&
						stepTwo.eventDate.hasClass("valid-field") == true &&
						stepTwo.eventTimeFrom.hasClass("valid-field") == true &&
						stepTwo.eventTimeTo.hasClass("valid-field") == true) {
						lockButtons(false);
					stepTwo.valid = true;
				}
			} else if(index == 2) {
				if(stepThree.numOfGuests.hasClass("valid-field") == true &&
					stepThree.specialRequests.hasClass("valid-field") == true &&
					stepThree.eventTerms.hasClass("valid-field") == true) {
					lockButtons(false);
				stepThree.valid = true;
			}
		}
	}
});
		});
	}

	jQuery("#eventsForm").submit(submitEvent);

	function submitEvent(e) {
		if(stepOne.valid === true &&
			stepTwo.valid === true &&
			stepThree.valid === true) {
			e.preventDefault();
		jQuery("#eventsForm").hide();
		let name = jQuery("#guestFName").val() + " " + jQuery("#guestLName").val();
		let email = jQuery("#guestEmail").val();
		let output = "Thank you, " + name + "! We have sent the details to your email: " + email;
		output += "<p> See you there! </p>";

		jQuery("#result").slideDown(300).html(output);
	}
}

	// check the select state of the event type
	// hide or show the specify field depending on the choice
	jQuery("#eventType").change(function(){
		var choice = parseInt(jQuery(this).val());
		if(choice === 4) {
			jQuery(".other-event").slideDown(300);
			jQuery("#otherEvent").removeClass("valid-field");
		} else {
			jQuery(".other-event").slideUp(300);
			jQuery("#otherEvent").addClass("valid-field");
		}
	});

	// Sitemap======//
	//sitemap expanding text
	$("#list-locations").hide();
	$(".sitemap-location").hide();
	$(".pageDescription").hide();

	$("h2").click(function(){
		$(this).next("p").toggle(300);
	});

	$("#location-description").click(function(){
		$("#list-locations").toggle(300);
	});

	$(".sitemap-location").hide();
	$("h3").click(function(){
		$(this).nextAll("li").toggle(300);
	});


	//giftcards validation logic

	//Gift Card Validation
    var formHandle = document.forms[0];

    formHandle.onsubmit = processForm;

    function processForm(){
        var fnameValue = document.forms[0].fname.value;
        var lnameValue = document.forms[0].lname.value;
        var emailValue = document.forms[0].buyerEmail.value;

        // regex taken from Mark's code below
        var emailRegEx = /([a-z|A-Z0-9._-])*@[a-z]+.[a-z]+/;
        //validation of empty fields and regex validation
        if(fnameValue === "" || fnameValue === null){
            fnameErrorMsg = document.getElementById("fnameError");
            fnameErrorMsg.innerHTML = "Enter your first name.";
            return false;
        }else if(lnameValue === "" || lnameValue === null){
            lnameErrorMsg = document.getElementById("lnameError");
            lnameErrorMsg.innerHTML = "Enter your last name.";
            return false;
        }else if(emailValue === "" || emailValue === null){
            emailErrorMsg = document.getElementById("emailError");
            emailErrorMsg.innerHTML = "Enter your e-mail.";
            return false;
        } 
        if(emailRegEx.test(emailValue) === false){
            fnameErrorMsg = document.getElementById("emailError");
            fnameErrorMsg.innerHTML = "Enter a valid e-mail.";
            return false;
        }
        //tried changing the message but it wasnt working out
        onsubmit = displayMsg;

        function displayMsg(){
        var giftCardForm = document.getElementById("giftcard-wrap");
        var confirm = document.getElementById("confirmation-msg");

        giftCardForm.style.display = "none";
        confirm.innerHTML = "<p>You have successfully purchased a gift card.</p>"
        }
    }

}); // end document ready

// events functions
// Function for the form Wizard
function formWizard(direction) {
	var steps = jQuery(".formWizard").length - 1; // I subtracted one to match the current index
	var activeIndex = jQuery(".formWizard.active");
	var index = activeIndex.index();
	var value = 0;

	if(direction == "next"){
		index = activeIndex.index() + 1;
		activeIndex.removeClass("active").next().toggleClass("active");
	}

	if(direction == "previous") {
		// store the current values
		index = activeIndex.index() - 1;
		activeIndex.removeClass("active").prev().toggleClass("active");
		// jump to previous step
		// present the previous values
		// check if user changed some info and validate again
	}

	// show or hide previous button;
	if(index > 0){
		jQuery("#prevButton").show();
	} else {
		jQuery("#prevButton").hide();
	}

	// check if it's the last active form. If it is, show submit button instead of next
	if(index === steps ) {
		jQuery("#nextButton").hide();
		jQuery("#submitButton").addClass("inline");
	} else {
		jQuery("#nextButton").show();
		jQuery("#submitButton").removeClass("inline");
	}

	console.log("index: " + index);
	console.log("steps: " + steps);
	console.log(direction);
}

function validateField(element, type) {
	let value = "";
	let msgTimeVal = 0;
	if(type == "eventDate") {
		value = element.datepicker().val();
	} else {
		value = element.val();
	}

	// choose validation
    // I made these regex through trial and error and with the help of this website:
    // name validation: regexr.com/43afd
    // email validation: regexr.com/43aeo
    // I did not make the phone number validation here's the link
    // https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s02.html
    // I did not do the time format validation I it found in on stack overflow
    // time validation: https://stackoverflow.com/questions/1494671/regular-expression-for-matching-time-in-military-24-hour-format

    switch(type) {
    	case "guestName":
    	validation = /([A-Z|a-z]*\s*[A-Z|a-z]*)[^0-9]/;
    	break;
    	case "guestEmail":
    	validation = /([a-z|A-Z0-9\.\_\-])*\@[a-z]+\.[a-z]+/;
    	break;
    	case "guestPhone":
    	validation = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})/;
    	break;
    	case "timeFormatFrom":
    	case "timeFormatTo":
    	validation = /^((((1[8-9])|(2[0-2])):?[0-5][0-9]))$/;
    	break;
    	default:
    	validation = "";
    	break;
    }

	// check if it's empty or null
	if(isEmpty(value)||isNull(value)) {
		showValidation(false, "empty", element, "Field cannot be empty");
		return false;
	} else {
		if(validation !== ""){
			if(validation.test(value)){
				// success
				showValidation(true, "valid", element, "");
				return true;
			} else {
				// fail
				showValidation(false, "invalid", element, "Field is invalid!");
				return false;
			}
		} else {
			// no further validation needed	as long as it's not empty
			showValidation(true, "valid", element);
			return true;
		}
	}
}

function showValidation(status, cClass, element, details) {

	if(status == true){
		element.removeClass("validation-error");
		element.addClass("valid-field");
		element.prev("span").text("").removeAttr('class');
		element.prev("span").addClass("validationMsg valid");
	} else {
		element.removeClass("valid-field");
		element.addClass("validation-error");
		element.prev("span").text(details).removeAttr("class");
		element.prev("span").addClass("validationMsg " + cClass);
		lockButtons(true);
	}
}

function lockButtons(status) {
	if(status === true){
		jQuery("#submitButton").prop("disabled", true);
		jQuery("#nextButton").attr("disabled", true)
	} else {
		jQuery("#submitButton").prop("disabled", false);
		jQuery("#nextButton").attr("disabled", false)
	}
}

// I used ternary oparator a.k.a shorthand if else on these functions
// because I only want to evaluate if it's true or false
function isEmpty(val) {
	var result = val === null ? true : false;
	return result;
}
function isNull(val) {
	var result = val === "" ? true : false;
	return result;
}

// contact functions
//JQuery Validation function for the contact form
function contactFormValidate() {
	var nameArray = [jQuery("#contactFirstName").val(), jQuery("#contactLastName").val()];
	var nameFlag = [true,true];
	var emailFlag = true;
	var status = "";
	var validationMsg = "";
	var contactInfo = {
		guestName: nameArray,
		email: jQuery("#contactEmail").val(),
		message: jQuery("#contactMessage").val()
	};

	// I made these regex through trial and error and with the help of this website:
	// name validation: regexr.com/43afd
	// email validation: regexr.com/43aeo
	var regex = {
		string: /([A-Z|a-z]*\s*[A-Z|a-z]*)[^0-9]/,
		email: /([a-z|A-Z0-9\.\_\-])*\@[a-z]+\.[a-z]+/
	};


	//cycle through the guestname Array
	for(var ctr=0; ctr < contactInfo.guestName.length; ctr++) {
		if(contactInfo.guestName[ctr] === "" || contactInfo.guestName[ctr] === null){
			// fill each flag with status
			nameFlag[ctr] = false;
		}
	}

	//check for email
	if(contactInfo.email === "" || contactInfo.email === null){
		emailFlag = false;
	}

	//validate the firstName & lastName
	for(var ctr = 0;ctr < contactInfo.guestName.length; ctr++) {
		console.log(contactInfo.guestName[ctr]);
		if(nameFlag[ctr]) {
			if (regex.string.test(contactInfo.guestName[ctr])) {
				//console.log('valid name');
			} else {
				//console.log('invalid name');
				status = (ctr === 0) ? "First name" : "Last name";
				validationMsg += status + " is invalid<br/>";
			}
		} else {
			// since we know that nameFlag variable either has 0 or 1 index
			// I used a ternary oparator A.K.A shorthand if else to store the string that is required
			status = (ctr === 0) ? "First name" : "Last name";
			validationMsg += status + " is required<br/>";
		}
	}

	//validate the email
	if(emailFlag){
		if (regex.email.test(contactInfo.email)) {
			//console.log('valid email');
		} else {
			validationMsg = "Email is invalid<br/>";
			jQuery("#contactEmail").next('.validationMsg').text(validationMsg);
		}
	} else {
		validationMsg += "Email is required<br/>";
	}

	//check for message area
	if(contactInfo.message === "" || contactInfo.message === null){
		validationMsg += "Please enter a message!<br/>";
	}

	// output the validation message if there is any
	validationMsg == "" ? console.log("no errors") : console.log(validationMsg);
	if(validationMsg === "" ) {
		let output = "<p>Thank you for contacting us! we will get back to you as soon as possible! We have sent a confirmation message to your email" + contactInfo.email + "</p>";
		jQuery("#contactForm").slideUp(200);
		jQuery("#errors").slideUp(200);
		jQuery("#result").removeClass("errors").slideDown(300).html(output);
	} else {
		jQuery("#errors").slideDown(300).html(validationMsg);
	}
	return false;
}


//----------LOCATIONS----------

//Scroll animation for Locations
$("#toLocations").click(function(){
	scrollToAnchor('locations');
})

//Etobicoke
//Hover animation for location one picture
$(document).ready(function(){
	$('#locationOne').hover(function(){
		$(this).stop().animate({
			opacity: .4
		}, 200);
		$('#addressEtobicoke').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressEtobicoke').addClass('hidden');
	});
});

//Hover animation for location one address
$(document).ready(function(){
	$('#addressEtobicoke').hover(function(){
		$('#addressEtobicoke').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressEtobicoke').addClass('hidden');
	});
});

//Hover animation for location one city name
$(document).ready(function(){
	$('#etobicokeTextLink').hover(function(){
		$('#addressEtobicoke').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressEtobicoke').addClass('hidden');
	});
});

//Mississauga
//Hover animation for location two picture
$(document).ready(function(){
	$('#locationTwo').hover(function(){
		$(this).stop().animate({
			opacity: .4
		}, 200);
		$('#addressMississauga').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressMississauga').addClass('hidden');
	});
});

//Hover animation for location two address
$(document).ready(function(){
	$('#addressMississauga').hover(function(){
		$('#addressMississauga').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressMississauga').addClass('hidden');
	});
});

//Hover animation for location two city name
$(document).ready(function(){
	$('#mississaugaTextLink').hover(function(){
		$('#addressMississauga').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressMississauga').addClass('hidden');
	});
});

//Markham
//Hover animation for location three picture
$(document).ready(function(){
	$('#locationThree').hover(function(){
		$(this).stop().animate({
			opacity: .4
		}, 200);
		$('#addressMarkham').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressMarkham').addClass('hidden');
	});
});

//Hover animation for location three address
$(document).ready(function(){
	$('#addressMarkham').hover(function(){
		$('#addressMarkham').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressMarkham').addClass('hidden');
	});
});

//Hover animation for location three city name
$(document).ready(function(){
	$('#markhamTextLink').hover(function(){
		$('#addressMarkham').removeClass('hidden');
	}, function(){
		$(this).stop().animate({
			opacity: 1
		}, 500);
		$('#addressMarkham').addClass('hidden');
	});
});

//Google Maps
//Referenced from
//https://developers.google.com/maps/documentation/javascript/adding-a-google-map#key
// Initialize and add the map
//For location one using Apache Burger
//5236 Dundas St W, Etobicoke, ON M9B 1A7
function initMap() {
	// The location of Apache
	var Apache = {lat: 43.639320, lng: -79.537840};
	// The map, centered at Apache
	var map = new google.maps.Map(
		document.getElementById('map'), {zoom: 13, center: Apache});
	// The marker, positioned at Apache
	var marker = new google.maps.Marker({position: Apache, map: map});
}

  // Initialize and add the map
//For location two using Szechuan Noodle House
//5236 Dundas St W, Etobicoke, ON M9B 1A7
function initMississaugaMap() {
	// The location
	var Noodle = {lat: 43.603740, lng: -79.590100};
	// The map, centered at location
	var mississaugaMap = new google.maps.Map(
		document.getElementById('mississaugaMap'), {zoom: 13, center: Noodle});
	// The marker, positioned at location
	var marker = new google.maps.Marker({position: Noodle, mississaugaMap: mississaugaMap});
}

    // Initialize and add the map
//For location two using Smash kitchen and bar
//4261 Highway 7 E, Markham, ON L3R 9W6
function initMarkhamMap() {
	// The location
	var Smash = {lat: 43.859040, lng: -79.314630};
	// The map, centered at location
	var markhamMap = new google.maps.Map(
		document.getElementById('markhamMap'), {zoom: 13, center: Smash});
	// The marker, positioned at location
	var marker = new google.maps.Marker({position: Smash, markhamMap: markhamMap});
}


//----------ABOUT----------
//Function for fading in paragraph headings on scroll
//Referenced from https://jsfiddle.net/tcloninger/e5qaD/
//Do this when the document is ready
$(document).ready(function(){
	//Scroll event listener, on scroll do this function
	$(window).scroll(function(){
		//For each element with the class hideAboutHeadings run this function
		$('.hideAboutHeadings').each(function(){
			//Calculate element height
			var object_bottom = $(this).position().top + $(this).outerHeight();
			//Calculate bottom of the window
			var window_bottom = $(window).scrollTop() + $(window).height();

			//If the object is visible in the window then it will run this fade animation
			if(window_bottom > object_bottom){
				$(this).animate({'opacity' : '1'}, 2500);
			}
		});
	});
});

//Same code as above but for the paragraph sections
$(document).ready(function(){
	//Scroll event listener, on scroll do this function
	$(window).scroll(function(){
		//For each element with the class hideAboutHeadings run this function
		$('.hideAboutParagraphs').each(function(){
			//Calculate element height
			var object_bottom = $(this).position().top + $(this).outerHeight();
			//Calculate bottom of the window
			var window_bottom = $(window).scrollTop() + $(window).height();

			//If the object is visible in the window then it will run this fade animation
			if(window_bottom > object_bottom){
				$(this).animate({'opacity' : '1'}, 3000);
			}
		});
	});
});


